import { useMemo } from 'react'
import { createStore, applyMiddleware, Store, AnyAction } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import * as types from './actionTypes'
import { State } from './types'

let store: Store | null

const initialState: State = {
  tasks: []
}

const reducer = (state = initialState, { type, payload }: AnyAction) => {
  switch (type) {
    case types.ADD_TASK:
      return { ...state, tasks: [...state.tasks, payload] };
    case types.SAVE_TASKS:
      return {...state, tasks: [...payload]}
    default:
      return state
  }
}

function initStore(preloadedState = initialState) {
  return createStore(
    reducer,
    preloadedState,
    composeWithDevTools(applyMiddleware())
  )
}

export const initializeStore = (preloadedState: State) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = null
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState: State) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}
