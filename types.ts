export interface Task {
  name: string
  id: string
  complete: boolean
}
export interface State {
  tasks: Task[]
}