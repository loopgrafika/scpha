import '../reset.css'
import { Provider } from 'react-redux'
import type { AppProps } from 'next/app'
import Head from 'next/head'
import { useStore } from '../store'

function MyApp({ Component, pageProps }: AppProps) {

  const store = useStore(pageProps.initialReduxState)

  return (
    <>
      <Head>
        <title>SCPHA - test</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Lorem ipsum dolor sit amet" />
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  )
}
export default MyApp
