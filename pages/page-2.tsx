import React, { useState } from 'react'
import { v1 as uuidv1 } from 'uuid'
import Image from 'next/image'

import { App, Main } from '../components/Layout'
import { Button, FormGroup } from '../components/Form'
import Header from '../components/Header'
import Footer from '../components/Footer'

import { useTasks } from '../hooks'
import TaskInput from '../components/TaskInput'

import plus from '../svg/plus.svg'
import { Task } from '../types'

export default function Page2() {
  const { tasks, setTasks, saveTasks, addTask } = useTasks()
  const [name, setName] = useState('')

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>, id: string) => {
    const newTasks = tasks.map((t: Task) => t.id === id ? { ...t, complete: e.target.checked } : t)
    setTasks(newTasks)
  }
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const handleOnSave = () => {
    saveTasks(tasks)
  }
  const handleOnAdd = () => {
    if(name) {
      addTask({
        name,
        id: uuidv1(),
        complete: false
      })
      setName('')
    }
  }

  return (
    <App>
      <Header/>
      <Main>
        <h1>Add tasks</h1>
        <FormGroup>
          <input type='text' onChange={handleNameChange} value={name} name='name'/>
          <Button type='primary' onClick={handleOnAdd} disabled={!name}>
            <Image src={plus} alt='' width={20} height={20} />
            Add
          </Button>
        </FormGroup>
        <ul>
          {tasks.map((task: Task, i: number) => (
            <TaskInput key={task.id} task={task} onChange={handleOnChange} index={i + 1}/>
          ))}
        </ul>
        {!!tasks.length && <Button type='primary' onClick={handleOnSave}>Mark Complete</Button>}
      </Main>
      <Footer />
      <style jsx>
        {`
          input {
            margin-right: 0.5rem;
          }
          ul {
            padding: 0.5rem 0;
          }
        `}
      </style>
    </App>
  )
}
