import Link from 'next/link'
import React from 'react'
import Footer from '../components/Footer'
import Header from '../components/Header'
import { App, Main } from '../components/Layout'
import Task from '../components/Task'

import { useTasks } from '../hooks'
import { Task as TaskType } from '../types'

export default function Page3() {
  const { tasks } = useTasks()

  return (
   <App>
      <Header/>
      <Main>
        <h1>List tasks</h1>
        {!!tasks.length ?
          <ol>
            {tasks.map((task: TaskType) => (
              <Task key={task.id} task={task} />
            ))}
          </ol>
        : <p>
            No tasks yet. Go to 
            <Link href='/page-2'>
              <a title='Page 2'> Page 2 </a>
            </Link>
            to add some.
          </p>
        }
      </Main>
      <Footer />
    </App>
  )
}
