import React from 'react'
import { App, Grid, Main } from '../components/Layout'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Card from '../components/Card'
import Hero from '../components/Hero'

const lorem = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem accusantium veritatis fugiat sit distinctio, eaque quis provident, inventore, sapiente perferendis omnis facilis aut laborum fugit qui accusamus nostrum pariatur error?`
const image = 'https://picsum.photos/id/870/300/300'

export default function Page2() {
  return (
    <App>
      <Header />
      <Main>
        <Grid col={2}>
          <Hero title='Banner for the Website' subtitle='Subtitle' image={image} url={'/'} />
          <Card title='Page 2' body={lorem} url='/page-2' />
          <Card title='Page 3' body={lorem} url='/page-3' />
        </Grid>
      </Main>
      <Footer />
    </App>
  )
}
