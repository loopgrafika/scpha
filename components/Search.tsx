import React from 'react'
import Image from 'next/image'
import search from '../svg/search.svg'

export default function Search() {
  return (
    <span className='wrapper'>
      <span className='image'>
        <Image src={search} alt='' />
      </span>
      <input />
      <style jsx>{`
        .wrapper {
          position: relative;
          margin-right: 0.5rem;
        }

        .image {
          position: absolute;
          top: 0.375rem;
          left: 0.375rem;
        }

        input {
          padding-left: 2rem;
        }
      `}
      </style>
    </span>
  )
}
