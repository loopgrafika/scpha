import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { FlushSpace } from './Layout'
import { BREAKPOINT } from '../constants'

interface Props {
  title: string
  image?: string
  subtitle?: string
  url?: string
}

export default function Hero({ image, title, subtitle, url }: Props) {
  return (
    <div className='wrapper'>
      {image && 
        <div className='image'>
          <Image src={image} alt={title} width={200} height={300} layout='responsive' />
        </div>
      }
      <div className='inner'>
        <h3>{subtitle}</h3>
        <h1>{title}</h1>
        <FlushSpace />
        {url && <Link href={url}>
          <a title={title}>Read More</a>
        </Link>}
      </div>
      <style jsx>
        {`
          .wrapper {
            display: flex;
            background-color: #222;
            color: #eee;
            grid-column: span 2;
          }
          .inner {
            display: flex;
            flex-direction: column;
            padding: 1rem;
            flex: 1 1 auto;
          }
          .image {
            flex: 1 1 25%;
            max-width: 300px;
            min-width: 160px;
          }
          h1 {
            font-size: 2rem;
            font-weight: 300;
            line-height: 1;
          }
          h3 {
            font-size: 1rem;
            font-weight: 300;
          }
          @media screen and (min-width: ${BREAKPOINT}px) {
            h1 {
              font-size: 3rem;
            }
            .inner {
              padding: 2rem;
            }
          }
        `}
      </style>
    </div>
  )
}