import React from 'react'
import Image from 'next/image'
import info from '../svg/info.svg'

import { Task } from '../types'

interface TaskProps {
  task: Task
}

export default function TaskComponent({ task }: TaskProps): JSX.Element {
  return (
    <li>
      <span>
        {task.name}
        {task.complete &&
          <span className='info'>
            <Image src={info} alt='' width={16} height={16}/>
            &nbsp;
            complete
          </span>}
      </span>
      <style jsx>
        {`
          li {
            align-items: center;
            margin-bottom: 0.25rem;
          }
          span {
            display: flex;
          }
          .info {
            margin-left: 0.5rem;
            display: flex;
            align-items: center;
            text-transform: uppercase;
            opacity: 0.5;
            font-size: 0.675rem;
          }
          `}
      </style>
    </li>
  )
}
