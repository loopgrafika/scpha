import React from 'react'
import Link from 'next/link'
import { BREAKPOINT } from '../constants'

interface Props {
  title: string
  body: string
  url?: string
}

export default function Card({ title, body, url }: Props) {
  return (
    <article>
      <h2>{title}</h2>
      <p>{body}</p>
      {url && <Link href={url}>
        <a title={title}>Read More</a>
      </Link>}
      <style jsx>
        {`
          article {
            background-color: #fff;
            padding: 1rem;
            grid-column: span 2;
            display: flex;
            flex-direction: column;
            justify-content: stretch;
            min-height: 200px;
          }

          p {
            flex: 1 0 auto;
          }

          @media screen and (min-width: ${BREAKPOINT}px) {
            article {
              grid-column: span 1;
              padding: 1.5rem;
            }
          }
        `}
      </style>
    </article>
  )
}
