import { useRouter } from 'next/router';
import Link from 'next/link'

const Nav = () => {
  const router = useRouter();

  return (
    <nav>
      <Link href='/'>
        <a className={router.pathname == '/' ? 'active' : ''} title='Page 1'>Page 1</a>
      </Link>
      <Link href='/page-2'>
        <a className={router.pathname == '/page-2' ? 'active' : ''} title='Page 2'>Page 2</a>
      </Link>
      <Link href='/page-3'>
        <a className={router.pathname == '/page-3' ? 'active' : ''} title='Page 3'>Page 3</a>
      </Link>
      <style jsx>
        {`
          a {
            display: inline-block;
            padding-right: 3rem;
            color: inherit;
            font-weight: 700;
          }
          a.active {
            color: #673ab7;
          }
          nav {
            flex: 0 0 auto;
          }
        `}
      </style>
    </nav>
  )
}

export default Nav