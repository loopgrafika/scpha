import { BREAKPOINT } from "../constants"

interface Props {
  children: React.ReactNode
}

interface GridProps extends Props {
  col?: number
}

interface ColProps extends Props {
  span?: number
  justify?: 'start' | 'end' | 'center' | 'stretch'
}

export const App = ({ children }: Props): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            padding: 0;
            margin: 0;
            display: flex;
            flex-flow: column wrap;
            align-items: stretch;
            justify-content: stretch;
            min-height: 100vh;
            background-color: #F2F2F2;
            overflow: scroll;
          }
        `}
      </style>
      <style jsx global>
        {`
          input {
            border-radius: 0.25rem;
            padding: 0.5rem 0.75rem;
            border: 1px solid #222;
          }
          input:not([type="checkbox"]) {
            min-height: 2.25rem;
          }
          [disabled] {
            opacity: 0.5 !important;
            cursor: default !important;
          }
          h1, h2, h3, h4 {
            margin-bottom: 0.5rem;
          }
          a {
            color: #673ab7;
          }
          img {
            object-fit: cover;
          }
        `}
      </style>
    </div>
  )
}

export const HeaderSection = ({ children }: Props): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            display: flex;
            flex-wrap: row;
            justify-content: space-between;
            align-items: center;
            padding: 1rem;
            margin: 0 auto;
            flex: 0 0 auto;
            width: 100%;
            background-color: #fff;
          }
          div + div {
            padding-top: 0;
          }
        `}
      </style>
    </div>
  )
}


export const FooterSection = ({ children }: Props): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            padding: 1rem 1rem 0;
            margin: 0 auto;
            max-width: 1044px;
            flex: 0 0 auto;
            width: 100%;
          }
        `}
      </style>
    </div>
  )
}

export const Main = ({ children }: Props): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            padding: 1rem 1rem 0;
            margin: 0 auto;
            max-width: 1044px;
            flex: 1 1 auto;
            width: 100%;
          }
        `}
      </style>
    </div>
  )
}

export const Container = ({ children }: Props): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            padding: 1rem 1rem 0;
            margin: 0 auto;
            max-width: 1044px;
            width: 100%;
          }
        `}
      </style>
    </div>
  )
}

export const FlushSpace = (): JSX.Element => {
  return (
    <div>
      <style jsx>
        {`
          div {
            flex: 1 0 auto;
          }
        `}
      </style>
    </div>
  )
}

export const Grid = ({ children, col = 1 }: GridProps): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            padding: 1rem 0;
            display: grid;
            gap: 1rem;
            grid-template-columns: repeat(${col <= 1 ? col : col / 2}, 1fr);
            flex: 1 1 auto;
          }

          @media screen and (min-width: ${BREAKPOINT}px) {
            div {
              grid-template-columns: repeat(${col}, 1fr);
            }
          }
        `}
      </style>
    </div>
  )
}

export const Col = ({ children, span = 1, justify = 'stretch' }: ColProps): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            grid-column: span ${span <= 1 ? span : span / 2};
            justify-self: ${justify}
          }
          @media screen and (min-width: ${BREAKPOINT}px) {
            div {
              grid-column: span ${span};
            }
          }
        `}
      </style>
    </div>
  )
}
