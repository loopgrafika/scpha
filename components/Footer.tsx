import Link from 'next/link'
import React from 'react'
import { FooterSection, Grid, Col } from './Layout'

export default function FooterSectionSection() {
  return (
    <FooterSection>
      <Grid col={4}>
        <Col span={2}>
          <div className='separator'>
            <h2>SCPHA</h2>
            <h4>Latest Blog Post</h4>
            <article>
              <h3>Ready To Get Started?</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, doloribus. Numquam nostrum aperiam dignissimos ab doloribus, ullam illo vel consectetur officia eius. Deleniti veritatis sit pariatur suscipit culpa sapiente reprehenderit.</p>
            </article>
          </div>
        </Col>
        <Col span={2}>
        <Grid col={2}>
          <Col span={1} justify='end'>
          <h4>Product</h4>
          <ul>
            <li>Product</li>
            <li>Product</li>
            <li>Product</li>
            <li>Product</li>
            <li>Product</li>
          </ul>
          </Col>
          <Col span={1} justify='end'>
          <h4>Company</h4>
          <ul>
            <li>Company</li>
            <li>Company</li>
            <li>Company</li>
            <li>Company</li>
            <li>Company</li>
          </ul>
          </Col>
        </Grid>
        <p className='align-right'>
          &copy; 2010 – 2021
          &nbsp; &nbsp;
          <Link href='/'>
            <a title='Privacy'>Privacy</a>
          </Link>{' – '}
          <Link href='/'>
            <a title='Terms'>Terms</a>
          </Link>
        </p>
        </Col>
      </Grid>
      <style jsx>{`
        .separator {
          margin-right: 1rem;
          padding-right: 1rem;
          border-right: 1px solid #222;
        }
        .align-right {
          text-align: right;
        }
      `}</style>
    </FooterSection>
  )
}
