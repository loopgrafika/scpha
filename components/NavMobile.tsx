import { useState } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import { Button } from './Form'
import chevronUp from '../svg/chevron-up.svg'
import chevronDown from '../svg/chevron-down.svg'

const Nav = () => {
  const [open, setOpen] = useState(false)
  return (
    <div className='wrapper'>
    <Button onClick={() => setOpen((prev) => !prev)}>
      <span className={open ? 'button active' : 'button'}>Menu</span>
      {open
        ? <Image src={chevronUp} alt='' width={20} height={20} />
        : <Image src={chevronDown} alt='' width={20} height={20} />
      }
    </Button>
    {open &&
      <nav>
        <Link href="/">
          <a title='Page 1'>Page 1</a>
        </Link>
        <Link href="/page-2">
          <a title='Page 1'>Page 2</a>
        </Link>
        <Link href="/page-3">
          <a title='Page 1'>Page 3</a>
        </Link>
      </nav>}
      <style jsx>
        {`
          a {
            display: inline-block;
            padding: 0.5rem 1rem;
            border-bottom: 1px solid #ddd;
            min-width: 12rem;
            color: inherit;
          }
          a:last-child {
            border-bottom: 0;
          }
          .button {
            font-weight: 700;
          }
          .active {
            color: #673ab7
          }
          nav {
            position: absolute;
            right: 1rem;
            display: flex;
            flex-direction: column;
            padding: 0;
            background-color: #fff;
            box-shadow: 0rem 0.25rem 0.5rem #00000022;
            z-index: 1;
          }
        `}
      </style>
    </div>
  )
}

export default Nav