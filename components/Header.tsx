import React, { useMemo } from 'react'
import { Container, FlushSpace, HeaderSection } from './Layout'
import { Button } from './Form'
import Nav from './Nav'
import { BREAKPOINT } from '../constants'
import Search from './Search'
import NavMobile from './NavMobile'

export default function HeaderSectionSection() {

  return (
    <div>
      <div className='desktop'>
        <HeaderSection>
          <h2>SCPHA</h2>
          <FlushSpace />
          <Search />
          <Button type='secondary'>Sign Up</Button>
        </HeaderSection>
        <Container>
          <Nav />
        </Container>
      </div>
      <div className='mobile'>
        <HeaderSection>
          <h2>SCPHA</h2>
          <FlushSpace />
          <Button type='secondary'>Sign Up</Button>
        </HeaderSection>
        <HeaderSection>
          <Search />
          <FlushSpace />
          <NavMobile />
        </HeaderSection>
      </div>
      <style jsx>{`
        h2 {
          margin: 0;
        }
        .desktop {
          display: none;
        }
        .mobile {
          display: block;
        }
        @media screen and (min-width: ${BREAKPOINT}px) {
          .desktop {
            display: block;
          }
          .mobile {
            display: none;
          }
        }
      `}</style>
    </div>
  )
}

