interface Props {
  children?: React.ReactNode
}

interface ButtonProps extends Props {
  type?: 'primary' | 'secondary'
  onClick?: () => void
  disabled?: boolean
}

export const Button = ({ children, type, onClick, disabled }: ButtonProps): JSX.Element => {
  let style = ''
  switch(type) {
    case 'primary':
      style = `
        background-color: #222;
        border-radius: 1.25rem;
        color: #eee;
      `
      break
    case 'secondary':
      style = `
        background-color: transparent;
        border-radius: 0.25rem;
        color: #222;
        border: 1px solid #222;
      `
      break
    default:
      style = `
        background-color: transparent;
        color: #222;
      `
  }
  return (
    <button onClick={onClick} disabled={disabled}>
      {children}
      <style jsx>
        {`
          button {
            cursor: pointer;
            display: inline-flex;
            align-items: center;
            border: 0;
            padding: 0.5rem 0.75rem;
            font-size: 1rem;
            ${style}
          }

          button:hover {
            opacity: 0.9;
          }

          button:active {
            opacity: 0.75;
          }
        `}
      </style>
    </button>
  )
}

export const FormGroup = ({ children }: Props): JSX.Element => {
  return (
    <div>
      {children}
      <style jsx>
        {`
          div {
            display: inline-flex;
            align-items: center;
          }
        `}
      </style>
    </div>
  )
}