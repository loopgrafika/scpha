import React from 'react'
import Image from 'next/image'
import { Task } from '../types'
import unchecked from '../svg/square.svg'
import checked from '../svg/check-square.svg'

interface TaskInputProps {
  task: Task
  index: number
  onChange: (e: React.ChangeEvent<HTMLInputElement>, id: string) => void
}

export default function TaskComponent({ task, index, onChange }: TaskInputProps): JSX.Element {
  return (
    <li key={task.id}>
      <label htmlFor={task.id}>
        {!task.complete
          ? <Image src={unchecked} alt='' width={20} height={20}/>
          : <Image src={checked} alt='' width={20} height={20}/>
        }
      </label>
      <input name={task.id} id={task.id} type='checkbox' checked={task.complete} onChange={e => onChange(e, task.id)}/>
      {index}
      .&nbsp;
      {task.name}
      <style jsx>
        {`
          li {
            margin-bottom: 0.25rem;
            display: flex;
            align-items: center;
          }
          input[type="checkbox"] {
            margin-right: 0.5rem;
            display: none;
          }
          label {
            display: flex;
            align-items: center;
            margin-right: 0.5rem;
          }
        `}
      </style>
    </li>
  )
}