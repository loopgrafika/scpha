import { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import type { Task, State } from './types'
import * as types from './actionTypes'

export const useTasks = () => {
  const dispatch = useDispatch()
  const stateTasks = useSelector((state: State) => state.tasks)

  const [tasks, setTasks] = useState(stateTasks)

  useEffect(() => {
    setTasks(stateTasks)
  }, [stateTasks])
  
  const addTask = (payload: Task) => dispatch({
    type: types.ADD_TASK,
    payload
  })

  const saveTasks = (payload: Task[]) => dispatch({
    type: types.SAVE_TASKS,
    payload
  })

  return { tasks, setTasks, addTask, saveTasks }
}
